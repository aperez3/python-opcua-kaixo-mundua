import time
import brickpi3
import logging
from sense_emu import SenseHat
import asyncio
import sys
import psutil
sys.path.insert(0, "..")


from asyncua import Server, ua
from asyncua.common.methods import uamethod

from color import color, senseHatColor



# Configure logger
logging.basicConfig(
    format=f'%(asctime)s {color.BLUE}%(levelname)-8s{color.END} %(message)s',
    level=logging.INFO,
    stream=sys.stdout
)
_logger = logging.getLogger('asyncua')


class IoT40_brickpi:
    def __init__(self):
        self.BP = brickpi3.BrickPi3()
        self.BP.set_sensor_type(self.BP.PORT_2, self.BP.SENSOR_TYPE.TOUCH)
        self.BP.set_sensor_type(self.BP.PORT_3, self.BP.SENSOR_TYPE.TOUCH)
        self.BP.set_sensor_type(self.BP.PORT_4, self.BP.SENSOR_TYPE.EV3_COLOR_COLOR)
        self.color = ["none", "Black", "Blue", "Green", "Yellow", "Red", "White", "Brown"]
        self.prev_s1_value = None
        self.prev_s2_value = None
        self.prev_s3_value = None
        self.prev_s4_value = None
        self.new_s1_value = None
        self.new_s2_value = None
        self.new_s3_value = None
        self.new_s4_value = None

    async def configure_opcua_server(self, server):
        # get Objects node, this is where we should put our custom stuff
        objects = server.nodes.objects

        # populating our address space with an object
        self.opcua_obj = await objects.add_object(
            ua.NodeId('https://mondragon.edu/object/brickpi3', 2),
            'BrickPi3'
        )

        # Read only variables
        self.opcua_touch_port_2 = await self.opcua_obj.add_variable(
            ua.NodeId('https://mondragon.edu/object/brickpi3/touch_port_2', 2),
            'touch_port_2',
            self.new_s2_value
        )
        self.opcua_touch_port_3 = await self.opcua_obj.add_variable(
            ua.NodeId('https://mondragon.edu/object/brickpi3/touch_port_3', 2),
            'touch_port_3',
            self.new_s3_value
        )
        self.opcua_detected_color = await self.opcua_obj.add_variable(
            ua.NodeId('https://mondragon.edu/object/brickpi3/color_port_4', 2),
            'detected_color',
            self.new_s4_value
        )

    async def update_opcua_values(self):
        if self.update_touch_value(self.BP.PORT_2):
            await self.opcua_touch_port_2.write_value(self.new_s2_value)
            logging.info(f'Touch (PORT_2):{self.new_s2_value}')
            self.prev_s2_value = self.new_s2_value

        if self.update_touch_value(self.BP.PORT_3):
            await self.opcua_touch_port_3.write_value(self.new_s3_value)
            logging.info(f'Touch (PORT_3):{self.new_s3_value}')
            self.prev_s3_value = self.new_s3_value

        if self.update_color_value(self.BP.PORT_4):
            await self.opcua_detected_color.write_value(self.new_s4_value)
            logging.info(f'New color: {self.new_s4_value}')
            self.prev_s4_value = self.new_s4_value

    def set_and_compare_values(self, port, new_value):
        prev_value = None
        if port == self.BP.PORT_1:
            prev_value = self.prev_s1_value
            self.new_s1_value = new_value
        elif port == self.BP.PORT_2:
            prev_value = self.prev_s2_value
            self.new_s2_value = new_value
        elif port == self.BP.PORT_3:
            prev_value = self.prev_s3_value
            self.new_s3_value = new_value
        elif port == self.BP.PORT_4:
            prev_value = self.prev_s4_value
            self.new_s4_value = new_value
        
        return False if prev_value == new_value else True 
            
    def update_touch_value(self, port):
        ret = True
        try:
            new_value = self.BP.get_sensor(port)
            ret = self.set_and_compare_values(port, new_value)
        except brickpi3.SensorError as error:
            logging.error(f"Error getting touch sensors: {error}")
            self.new_touch_values = [-1,-1]
        return ret

    def update_color_value(self, port):
        ret = True
        try:
            new_color = self.color[self.BP.get_sensor(port)]
            ret = self.set_and_compare_values(port, new_color)
        except brickpi3.SensorError as error:
            logging.error(f"Color sensor error: {error}")
            self.new_detected_color = "error"
        
        return ret


class IoT40_sense_emu:
    def __init__(self):
        self.joystick_action = '-'
        self.joystick_direction = '-'
        self.sense = SenseHat()

    async def configure_opcua_server(self, server):
        # get Objects node, this is where we should put our custom stuff
        objects = server.nodes.objects

        # populating our address space with an object
        self.opcua_obj = await objects.add_object(
            ua.NodeId('https://mondragon.edu/object/sense_emu', 2),
            'Sense Emu'
        )

        # Read only variables
        self.temperature = await self.opcua_obj.add_variable(
            ua.NodeId('https://mondragon.edu/object/sense_emu/temperature', 2),
            'temperature',
            self.sense.get_temperature()
        )
        self.humidity = await self.opcua_obj.add_variable(
            ua.NodeId('https://mondragon.edu/object/sense_emu/humidity', 2),
            'humidity',
            self.sense.get_humidity()
        )
        self.pressure = await self.opcua_obj.add_variable(
            ua.NodeId('https://mondragon.edu/object/sense_emu/pressure', 2),
            'pressure',
            self.sense.get_pressure()
        )
        self.joystick_direction = await self.opcua_obj.add_variable(
            ua.NodeId('https://mondragon.edu/object/sense_emu/joystick_action', 2),
            'joystick_action',
            '-'
        )
        self.joystick_action = await self.opcua_obj.add_variable(
            ua.NodeId('https://mondragon.edu/object/sense_emu/joystick_direction', 2),
            'joystick_direction',
            '-'
        )

        # Writable variables
        self.led_matrix = await self.opcua_obj.add_variable(
            ua.NodeId('https://mondragon.edu/object/sense_emu/let_matrix', 2),
            'LEDMatrix',
            [senseHatColor.BLACK for i in range(64)]
        )
        await self.led_matrix.set_writable()

        # Add a method
        await objects.add_method(
            ua.NodeId('https://mondragon.edu/methods/set_let_matrix', 2),
            ua.QualifiedName('Set LED Matrix', 2),
            self.set_led_matrix,
            [ua.VariantType.String],
            [ua.VariantType.String]
        )

        self.sense.stick.direction_any = self.joystick_event

    async def update_opcua_values(self):
        await self.temperature.write_value(self.sense.get_temperature())
        await self.humidity.write_value(self.sense.get_humidity())
        await self.pressure.write_value(self.sense.get_pressure())
    
    def joystick_event(self, event):
        print(f'Joystick: {event.action} | {event.direction}')

        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        loop.run_until_complete(self.joystick_direction.write_value(event.direction))
        loop.run_until_complete(self.joystick_action.write_value(event.action))
    

    @uamethod
    def set_led_matrix(self, parent, value):
        self.sense.show_message(value, text_colour=[255,0,0])

class IoT40_computer:
    def __init__(self):
        return

    async def configure_opcua_server(self, server):
        # get Objects node, this is where we should put our custom stuff
        objects = server.nodes.objects

        # populating our address space with an object
        self.opcua_obj = await objects.add_object(
            ua.NodeId('https://mondragon.edu/object/computer', 2),
            'Computer'
        )

        # Read only variables
        self.cpu = await self.opcua_obj.add_variable(
            ua.NodeId('https://mondragon.edu/object/computer/cpu_percent', 2),
            'cpu_percent',
            psutil.cpu_percent()
        )
        self.ram = await self.opcua_obj.add_variable(
            ua.NodeId('https://mondragon.edu/object/computer/ram_percent', 2),
            'ram_percent',
            psutil.virtual_memory().percent
        )

        # Writable variables
        self.write_me = await self.opcua_obj.add_variable(
            ua.NodeId('https://mondragon.edu/object/computer/write_me', 2),
            'WriteMe',
            6.7
        )
        await self.write_me.set_writable() # Set MyVariable to be writable by clients

        # Property
        self.agurra = await self.opcua_obj.add_property(
            ua.NodeId('https://mondragon.edu/object/computer/agurra', 2),
            "Agurra",
            "Kaixo MUndua!"
        )

    async def update_opcua_values(self): 
        new_val = await self.write_me.get_value() + 0.1
        await self.write_me.write_value(new_val)

        new_cpu = psutil.cpu_percent()
        await self.cpu.write_value(new_cpu)

        new_ram = psutil.virtual_memory().percent
        await self.ram.write_value(new_ram)

@uamethod
def echo(parent, value):
    return f'{value} {value}'


async def main():
    server = Server()
    await server.init()
    server.set_endpoint('opc.tcp://0.0.0.0:4840/opcua_server/')
    server.set_server_name("EJIE 2021 IoT Server")

    # setup our own namespace
    uri = 'http://examples.mondragon.edu/opcua'
    idx = await server.register_namespace(uri)

    # get Objects node, this is where we should put our custom stuff
    objects = server.nodes.objects

    # add an empty folder
    await objects.add_folder(idx, "MyEmptyFolder")


    # Add a method
    await objects.add_method(
        ua.NodeId('https://mondragon.edu/methods/echo', 2),
        ua.QualifiedName('Echo', 2),
        echo,
        [ua.VariantType.String],
        [ua.VariantType.String]
    )

    bp = IoT40_brickpi()
    await bp.configure_opcua_server(server)

    cmptr = IoT40_computer()
    await cmptr.configure_opcua_server(server)

    semu = IoT40_sense_emu()
    await semu.configure_opcua_server(server)
    
    _logger.info(f'{color.BOLD}Starting server!{color.END}')

    # Update values
    async with server:
        while True:
            await asyncio.sleep(1/60)
            await bp.update_opcua_values()
            await cmptr.update_opcua_values()
            await semu.update_opcua_values()


if __name__ == '__main__':
    asyncio.run(main())
