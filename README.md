# Python Opcua Kaixo Mundua

Testing asyncua for OPC-UA client and server development.

## Virtual environment

### Prepare virtual environment

* Create venv and activate it.

```console
python3 -m venv ./venv
source venv/bin/activate
```
* you should see ```(venv)``` a the begginging if everything went OK. 
* Install requirements.
```console
(venv) foo@bar: pip install -r requirements.txt
```


### Deactivate python venv
To deactivate the virtual environment just type:
```console
(venv) foo@bar:~$ deactivate
```

## Run application

Run the server:
```bash
python opcua_server.py
```

This will open an OPC-UA server on port 4840 (``opc.tcp://0.0.0.0:4840/opcua_server/```).

You can 