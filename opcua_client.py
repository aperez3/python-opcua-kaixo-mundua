import asyncio
import sys
from asyncua import Client, Node, ua
import logging
from color import color

# Configure logger
logging.basicConfig(
    format=f'%(asctime)s {color.BLUE}%(levelname)-8s{color.END} %(message)s',
    level=logging.INFO,
    stream=sys.stdout
)
_logger = logging.getLogger('asyncua')

# OPC-UA namespaces and nodes to read
default_namespace_uri = 'http://examples.mondragon.edu/opcua'
nodes = [
    {
        'namespace_uri': default_namespace_uri,
        'human_readable_name': 'agurra',
        'node_name': 'https://mondragon.edu/object/computer/agurra'
    },
    {
        'namespace_uri': default_namespace_uri,
        'human_readable_name': 'Write me',
        'node_name': 'https://mondragon.edu/object/computer/write_me'
    },
    {
        'namespace_uri': default_namespace_uri,
        'human_readable_name': 'CPU Percent',
        'node_name': 'https://mondragon.edu/object/computer/cpu_percent'
    },
    {
        'namespace_uri': default_namespace_uri,
        'human_readable_name': 'RAM Percent',
        'node_name': 'https://mondragon.edu/object/computer/ram_percent'
    },
    {
        'namespace_uri': default_namespace_uri,
        'human_readable_name': 'Battery Percent',
        'node_name': 'https://mondragon.edu/object/computer/battery_percent'
    },
]

echo_method = {
        'namespace_uri': default_namespace_uri,
        'human_readable_name': 'Echo2',
        'node_name': 'https://mondragon.edu/object/computer/echo',
        'parent_node_name': 'https://mondragon.edu/object/computer'
}


async def main():
    url = 'opt.tcp://localhost:4840/'
    async with Client(url=url) as client:
        idx = await client.get_namespace_index(uri=default_namespace_uri)

        # Call echo2 method ########################################################################
        parent_node = client.get_node(f'ns={idx};s={echo_method["parent_node_name"]}')
        response = await parent_node.call_method(
            f'{idx}:{echo_method["human_readable_name"]}',
            "Testing echo"
        )
        _logger.info(f'Echoed: {response}')
        # Reads ####################################################################################
        for node in nodes:
            node_name = node['node_name']
            namespace_uri = node['namespace_uri']
            human_readable_name = node['human_readable_name']
            value = await get_value(client, node_name, namespace_name=namespace_uri)
            _logger.info(
                f'{color.BOLD}{human_readable_name}{color.END} => {color.BOLD}{value}{color.END}'
            )

        # Subscriptions ############################################################################
        handler = SubscriptionHandler()
        subscription = await client.create_subscription(500, handler)
        subs_nodes = [
            client.get_node(f'ns={idx};s=https://mondragon.edu/object/computer/write_me'),
            client.get_node(f'ns={idx};s=https://mondragon.edu/object/computer/ram_percent'),
            client.get_node(f'ns={idx};s=https://mondragon.edu/object/computer/cpu_percent'),
            client.get_node(f'ns={idx};s=https://mondragon.edu/object/computer/battery_percent'),
            client.get_node(ua.ObjectIds.Server_ServerStatus_CurrentTime)
        ]
        # We subscribe to data changes for two nodes (variables).
        await subscription.subscribe_data_change(subs_nodes)
        # We let the subscription run for ten seconds
        await asyncio.sleep(10)
        # We delete the subscription (this un-subscribes from the data changes of the 2 variables).
        # This is optional since closing the connection will also delete all subscriptions.
        await subscription.delete()
        # After one second we exit the Client context manager - this will close the connection.
        await asyncio.sleep(1)


async def get_value(client, s, namespace_name=None, ns=None):
    if ns is None:
        ns = await client.get_namespace_index(namespace_name)
    node = client.get_node(f'ns={ns};s={s}')
    return await node.read_value()


class SubscriptionHandler:

    def datachange_notification(self, node: Node, val, data):
        """
        Callback for asyncua Subscription.
        This method will be called when the Client received a data change message from the Server.
        """
        _logger.info(f'Datachange notification: {color.BOLD}{node} => {val}{color.END}')


if __name__ == '__main__':
    asyncio.run(main())
