import logging
from sense_emu import SenseHat

import asyncio
import sys
import psutil
sys.path.insert(0, "..")

from asyncua import Server, ua
from asyncua.common.methods import uamethod

from color import color, senseHatColor

# Configure logger
logging.basicConfig(
    format=f'%(asctime)s {color.BLUE}%(levelname)-8s{color.END} %(message)s',
    level=logging.INFO,
    stream=sys.stdout
)
_logger = logging.getLogger('asyncua')

joystick_action = None
joystick_direction = None


@uamethod
def set_led_matrix(parent, value):
    # How to get sense?
    return f'Set LED Matrix not implemented yet'


async def main():
    global joystick_action, joystick_direction
    sense = SenseHat()
    server = Server()
    await server.init()
    server.set_endpoint('opc.tcp://0.0.0.0:4840/opcua_server/sense_emu/')
    server.set_server_name("Sense Emu OPCUA")

    # setup our own namespace
    uri = 'http://examples.mondragon.edu/opcua/sense_emu'
    idx = await server.register_namespace(uri)

    # get Objects node, this is where we should put our custom stuff
    objects = server.nodes.objects

    # populating our address space with an object
    myobj = await objects.add_object(
        ua.NodeId('https://mondragon.edu/object/sense_emu', 2),
        'Sense Emu'
    )

    # Read only variables
    temperature = await myobj.add_variable(
        ua.NodeId('https://mondragon.edu/object/sense_emu/temperature', 2),
        'temperature',
        sense.get_temperature()
    )
    humidity = await myobj.add_variable(
        ua.NodeId('https://mondragon.edu/object/sense_emu/humidity', 2),
        'humidity',
        sense.get_humidity()
    )
    pressure = await myobj.add_variable(
        ua.NodeId('https://mondragon.edu/object/sense_emu/pressure', 2),
        'pressure',
        sense.get_pressure()
    )
    joystick_direction = await myobj.add_variable(
        ua.NodeId('https://mondragon.edu/object/sense_emu/joystick_action', 2),
        'joystick_action',
        '-'
    )
    joystick_action = await myobj.add_variable(
        ua.NodeId('https://mondragon.edu/object/sense_emu/joystick_direction', 2),
        'joystick_direction',
        '-'
    )

    # Writable variables
    led_matrix = await myobj.add_variable(
        ua.NodeId('https://mondragon.edu/object/sense_emu/let_matrix', 2),
        'LEDMatrix',
        [senseHatColor.BLACK for i in range(64)]
    )
    await led_matrix.set_writable() # Set MyVariable to be writable by clients

    # Add a method
    await objects.add_method(
        ua.NodeId('https://mondragon.edu/methods/set_let_matrix', 2),
        ua.QualifiedName('Set LED Matrix', 2),
        set_led_matrix,
        [ua.VariantType.String],
        [ua.VariantType.String]
    )
    _logger.info(f'{color.BOLD}Starting server!{color.END}')

    # Update values
    sense.stick.direction_any = joystick_event
    async with server:
        while True:
            await asyncio.sleep(0.5)
            await temperature.write_value(sense.get_temperature())
            await pressure.write_value(sense.get_pressure())
            # await joystick_d.write_value(joystick_direction)
            # await joystick_a.write_value(joystick_action)


def joystick_event(event):
    global joystick_action, joystick_direction
    print(f'{event.action} | {event.direction}')
    # joystick_action = event.action
    # joystick_direction = event.direction

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(joystick_direction.write_value(event.direction))
    loop.run_until_complete(joystick_action.write_value(event.action))


if __name__ == '__main__':
    asyncio.run(main())
